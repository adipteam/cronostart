/*
 * File:   main.c
 * Author: MPLAB
 *
 * Created on July 8, 2016, 4:52 PM
 */
#include <xc.h>
#include <pic18f46k22.h>
#include "config.h"

//The main program starts from address 0x300
//The interrupts are redirected to addresses 0x308 and 0x3018

char getEEPROMStatus();
void readEEPROMFull();
void setEEPROMStatus(char s);
void writeBlockToProgramMemory();

//The Flash program memory is written using 64 bytes blocks
char programBlock[64];
unsigned int programAddress=0x300;

void interrupt HighIsr(void){
    asm("GOTO 0x308");
}

void interrupt low_priority LowIsr(void){
    asm("GOTO 0x318");
}

void i2c_wfc(){ //Wait for MSSP
    //Wait until the start/stop bit or data was sent
    while(SSP2IF==0){}
    SSP2IF=0;
}

void main(void) {    
    char s;
    
    ANSA1=0;TRISA1=0;LATA1=0; //Configure pin for control
    ANSA0=0;TRISA0=0;LATA0=0; //Configure pin for control
    
    //Configure I2C port 2
    ANSELDbits.ANSD0=0;
    ANSELDbits.ANSD1=0;
    SSPM32=1;SSPM22=0;SSPM12=0;SSPM02=0; //I2C Master mode, clock = FOSC / (4 * (SSPxADD+1)) - SSPxADD values of 0, 1 or 2 are not supported for I2C mode.
    //SSP2ADD=0x27; //FOSC=16Mhz. Fclock=100Khz.
    SSP2ADD=0x09; //FOSC=16Mhz. Fclock=400Khz.
    SSPEN2=1; //Enable I2C
    
    s=getEEPROMStatus();
    if(s==0x01){ //Start firmware update
        readEEPROMFull();
    }
    
    asm("GOTO 0x300");
    do{        
    }while(1);
}

//Read the first byte in the external memory.
//This is the memory status.
// 0x01 : EEPROM contains a valid firmware that can be used to update main program memory.
char getEEPROMStatus(){
    char r=0;
    SEN2=1;i2c_wfc(); //Send start bit
    SSP2BUF=0b10100000;i2c_wfc(); //Send slave address with write bit
    if(ACKSTAT2==0){ //Slave responded to address
        SSP2BUF=0x00;i2c_wfc(); //Memory address high byte
        SSP2BUF=0x00;i2c_wfc(); //Memory address low byte
        RSEN2=1;i2c_wfc(); //Repeated start
        SSP2BUF=0b10100001;i2c_wfc(); //Send slave address again with read bit
        if(ACKSTAT2==0){ //Slave responded to address
            RCEN2=1;i2c_wfc(); //Get byte from slave
            r=SSP2BUF;
            ACKDT2=1; //NACK - no more data needed
            ACKEN2=1;i2c_wfc(); //Send ACK bit
        }
    } 
    PEN2=1;i2c_wfc(); //Stop bit
    
    return r;
}

//Modify the EEPROM memory status (located on the first byte at 0x0000)
void setEEPROMStatus(char s){
    SEN2=1;i2c_wfc(); //Send start bit
    SSP2BUF=0b10100000;i2c_wfc(); //Send slave address with write bit
    if(ACKSTAT2==0){ //Slave responded to address
        SSP2BUF=0x00;i2c_wfc(); //Memory address high byte
        SSP2BUF=0x00;i2c_wfc(); //Memory address low byte
        SSP2BUF=s;i2c_wfc(); //Status
    }
    PEN2=1;i2c_wfc(); //Stop bit
}

void readEEPROMFull(){
    char r=0;
    char bp=0; //position in programBlock[64]
    unsigned int i=0x300;
    SEN2=1;i2c_wfc(); //Send start bit
    SSP2BUF=0b10100000;i2c_wfc(); //Send slave address with write bit
    if(ACKSTAT2==0){ //Slave responded to address
        SSP2BUF=0x03;i2c_wfc(); //Memory address high byte
        SSP2BUF=0x00;i2c_wfc(); //Memory address low byte
        RSEN2=1;i2c_wfc(); //Repeated start
        SSP2BUF=0b10100001;i2c_wfc(); //Send slave address again with read bit
        if(ACKSTAT2==0){ //Slave responded to address
            do{
                RCEN2=1;i2c_wfc(); //Get byte from slave
                r=SSP2BUF;
                programBlock[bp]=r;
                if(bp==63){//block is full
                    writeBlockToProgramMemory();
                    bp=0;
                    programAddress+=64; //program memory address goes to the next block
                    LATA0=1;LATA0=0;
                } else{
                    bp++;
                }
                i++;
                if(i<0xFFFF){
                    ACKDT2=0; //ACK - more data needed
                    ACKEN2=1;i2c_wfc(); //Send ACK bit
                }else{
                    ACKDT2=1; //NACK - no more data needed
                    ACKEN2=1;i2c_wfc(); //Send ACK bit
                }
                LATA1=1;LATA1=0;
            }while(i<0xFFFF);
        }
        
    } 
    PEN2=1;i2c_wfc(); //Stop bit
    
    setEEPROMStatus(0x02);
}

//Write the global block programBlock[64] to program memory
void writeBlockToProgramMemory(){
    char i;
    
    //Erase  
    TBLPTR=programAddress;
    EEPGD=1;
    CFGS=0;
    WREN=1;
    FREE=1;
    EECON2=0x55;
    EECON2=0xAA;
    WR=1;
    
    //Write to holding register
    TBLPTR=programAddress;
    for(i=0;i<64;i++){
        TABLAT=programBlock[i];
        asm("TBLWT*+");
    }
    
    //Program the holding register
    TBLPTR=programAddress;
    EEPGD=1;
    CFGS=0;
    WREN=1;
    FREE=0;
    EECON2=0x55;
    EECON2=0xAA;
    WR=1;
}