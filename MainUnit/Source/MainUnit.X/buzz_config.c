#include "main.h"

void buzz_config(){
    // @16Mhz
    // PR2 is a 8bit register. The highest value means the lowest frequency.
    // For PR2=0xFF => 976 Hz.
    // The duty cycle is dependant on the PR2, so CCPR1L, DC1B1 and DC1B0 (which together form a 10bit number) must be change in accordance with PR2
    // For the buzzer we need a duty cycle of 50% 

    ANSC2=0;TRISC2=1; //Set CCP1 as input to disable it's output (buzzer off)
    C1TSEL1=0;C1TSEL0=0; //PWM modes use Timer2
    CCP1M3=1;CCP1M2=1; //11xx = PWM mode
    PR2=200; //PWM Period
    CCPR1L=0b01100100;DC1B1=1;DC1B0=0; //DUTY CYCLE RATIO; calculated using CCPRxL:CCPxCON<5:4>
}