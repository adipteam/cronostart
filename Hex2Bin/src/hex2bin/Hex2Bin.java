/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hex2bin;

/**
 *
 * @author MPLAB
 */
public class Hex2Bin {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HEXTool htool;
        
        if(args.length!=2){
            System.out.println("Invalid parameters.");
            System.out.println("Ex. Hex2Bin.jar file.hex file.bin");
            return;
        }
        
        htool=new HEXTool(args[0],args[1]);
        htool.procHEX();
        System.out.println("Done.");
    }
    
}
