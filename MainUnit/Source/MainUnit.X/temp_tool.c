#include "main.h"

/*This command initiates a single temperature conversion.
Following the conversion, the resulting thermal data is
stored in the 2-byte temperature register in the scratchpad
memory and the DS18S20 returns to its low-power
idle state.*/
void temp_convertTemperature(){
    char r;
    r=temp_1wireReset();
    if(r==0){ //Presence detected
        temp_1wireReadByte(0xCC);    
        temp_1wireReadByte(0x44);
    }
}