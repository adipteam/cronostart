#include "main.h"

//Create a reset pulse and returns the presence pulse
//The presence pulse is 0 if some device on the line responded
char temp_1wireReset(){
    char r;
    TRISC0=0;__delay_us(500);TRISC0=1; //Reset pulse
    __delay_us(30); //Wait for thermometer to respond
    r=PORTCbits.RC0; //Temperature port
    __delay_us(480); //Wait for timeslot to complete
    return r;
}

//Send a byte on the 1wire line
void temp_1wireWrite(char b){
    //Data is sent LSB first
    for(char i=0;i<8;i++){
        if(bit_is_set(b,i)){
            temp_1wireWrite1();
        } else {
            temp_1wireWrite0();
        }
    }
}

//Write 1 on the 1wire line
void temp_1wireWrite1(){
    TRISC0=0;__delay_us(5);TRISC0=1;
    __delay_us(60); //End min 60us time slot    
    __delay_us(2); //Recovery time
}

//Write 0 on the 1wire line
void temp_1wireWrite0(){
    TRISC0=0;
    __delay_us(65); //End min 60us time slot    
    TRISC0=1;
    __delay_us(2); //Recovery time
}

//Read a byte from the 1wire line
char temp_1wireReadByte(){
    char r=0;
    char rb;
    
    for(char i=0;i<8;i++){
        TRISC0=0;__delay_us(5);TRISC0=1;
        __delay_us(5);
        rb=PORTCbits.RC0;
        __delay_us(60); //End min 60us time slot  
        __delay_us(2); //Recovery time
        
        if(rb==1){
            sbi(r,i);
        }
    }
    
    return r;
}