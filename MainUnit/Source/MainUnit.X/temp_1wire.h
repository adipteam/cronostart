/* 
 * File:   temp_1wire.h
 * Author: MPLAB
 *
 * Created on July 27, 2016, 11:15 AM
 */

#ifndef TEMP_1WIRE_H
#define	TEMP_1WIRE_H

char temp_1wireReset();
void temp_1wireWrite(char b);
void temp_1wireWrite1();
void temp_1wireWrite0();
char temp_1wireReadByte();

#endif	/* TEMP_1WIRE_H */

