/*
 * File:   main.c
 * Author: MPLAB
 *
 * Created on July 7, 2016, 4:46 PM
 */


#include <xc.h>
#include <pic18f46k22.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stddef.h>
#include "config.h"

//Interrupts
void interrupt HighIsr(void){
    
}

void interrupt low_priority LowIsr(void){
    
}

//Function used by printf function
void putch(char d){
    do{}while(TX1IF==0); //A character is being held for transmission in the TXREG. Loop until it is transmitted.
     TX1REG=d;
}

void main(void) {
    IRCF2=1;IRCF1=1;IRCF0=1; //HFINTOSC (16 MHz)
    ANSA1=0;TRISA1=0;LATA1=0; //Configure pin for control 
    
    //UART config
    ANSC6=0;ANSC7=0; // Tx/Rx pins set as digital
    TRISA6=1; TRISA7=1; // Tx/Rx pins as input
    BRGH1=1;BRG161=1;SPBRG1=68; // 57600bps at 16Mhz
    TXEN1=1; //Transmission is enabled
    //CREN1=1; //Reception is enabled
    SYNC1=0; //Asynchronous
    SPEN1=1; //Enable USART
    
    printf("Cotoi");
    
    do{
        
    }while(1);
}
