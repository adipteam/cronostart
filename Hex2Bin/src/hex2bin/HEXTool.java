/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hex2bin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 *
 * @author MPLAB
 */
public class HEXTool {
    String pathIn;
    String pathOut;
    byte[] memory=new byte[0xFFFF];
    boolean doNotProcessLine=false;
    
    public HEXTool(String in,String out){
        Arrays.fill(memory, (byte)0xFF);
        pathIn=in;
        pathOut=out;
    }
    
    public void procHEX() {
        doNotProcessLine=false;
        try (BufferedReader br = new BufferedReader(new FileReader(pathIn))) {
            String line;
            byte[] ln;
            while ((line = br.readLine()) != null) {
                line=line.replace(":",""); //Remove leading ":" from every line
                ln=hexStringToByteArray(line);
                System.out.println(line);
                processLine(ln);
            }            
            Path op = Paths.get(pathOut);
            Files.write(op, memory);
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
    void processLine(byte[] ln){
        int l=ln[0]; //Position 0 is data length
        int adr=0;
        int t=ln[3]; //Data type. 0x00:DATA
        
        if(doNotProcessLine) return;
        
        //Position 1,2 is address
        adr=ln[1];
        adr=adr<<8;
        adr+=ln[2];
        
        if(t==0){
            //Parse data bytes
            //The commands are stored in Flash using words little-endian.
            //In HEX file the words are big-endian, so bellow we take the bytes two at a time and switch them.
            for(int i=4;i<(4+l);i+=2){
                //If the line data is not executable code (it might be some const data) the total number of bytes might not be even (multiple words put together have allways an even number of bytes).
                //In the case of odd number of bytes, i+1 might go beyond the total bytes so we have to be sure that we don't process that byte.
                if((i+1)<(4+l)) memory[adr+1]=ln[i+1];
                memory[adr]=ln[i];
                adr+=2;
            }
        } else {
            doNotProcessLine=true; //If a line which is not data is encountered that means program data is over. (the following data are config bytes, user bytes etc.)
        }
    }
    
    byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                                 + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}
