#include "main.h"

void buzz_doWork(){
    if(timer3_buzz==0){
        TRISC2=1; //PWM output disable, buzzer off
    }
}

//The buzzer uses Timer3 configured with prescale 1:1
//This is about 61 overflows per second @16Mhz
//The buzzer will be on for T number of overflows
void buzz_doBuzz(int t){
    TRISC2=0; //Activate PWM, buzzer on
    timer3_buzz=t;
}