#include "main.h"

void temp_config(){
    //The pin is default set to input so the line will float due to weak pull-up resistor. 
    //The output value set to zero.
    //When the pin is set to output it will pull the line to the ground.
    //If the port is changed (other than RC0), all the 1wire functions must be changed.
    TRISC0=1;LATC0=0;  
}