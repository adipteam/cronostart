This application transforms the compiled HEX file to a program memory image.

This image can be downloaded on a CronoStart external EEPROM memory and then written into the main program memory. 